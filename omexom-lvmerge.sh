#!/bin/sh

# Installation de LabViewGitEnv
mkdir -p /usr/local
git clone https://christophe_orsini@bitbucket.org/christophe_orsini/labviewgitenv.git /usr/local
cd /usr/local && git checkout master

# Configuration de LabViewGitEnv
./bin/LVInit.sh --system

# Pause pour editer le LVConfig.sh
echo "Corriger le fichier /usr/local/etc/LVConfig.sh avec votre version de LabView..."
echo " "
echo "Ouvrir cmd.exe en tant qu'administrateur et entrer la commande :"
echo "setx Path %Path%;GIT_INSTALL_PATH\local\bin /m"
echo "où GIT_INSTALL_PATH devrait etre C:\ProgramFiles\Git"
echo " "
echo "Configurer Git Extention avec Mergetool command ="
echo "\"C:/Program Files/Git/usr/local/bin/LVMergeWrapper.bat\" \"$BASE\" \"$LOCAL\" \"$REMOTE\"" 